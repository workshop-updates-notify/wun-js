

var wunString = function() {};

wunString.prototype.isNumeric = function (input) { 
    return ((typeof input == "number" && !isNaN(input)) || 
            (Number(input) != NaN));
};
wunString.prototype.isNumberOnlyText = function (input) { 
    if (typeof input == "string") {
        var foundAlpha = false;
        var numbers = '0123456789';
        for(var i = 0; i < input.length; i++) {
            if (!numbers.includes(input[i])) {
                foundAlpha = true;
                break;
            }
        }
        return !foundAlpha; 
    }
    return false;
};

module.exports = new wunString(); 
const mysql = require('mysql'); 
var util = require('util'); 

var wunData = function() {};

wunData.prototype.getConnection = async function () { 
    var connection = mysql.createConnection({
        host     : 'localhost',
        user     : 'demouser',
        password : 'Usa1234!',
        database : 'wundb'
    });
    connection.query = util.promisify(connection.query);
    return new Promise(resolve => { resolve(connection); });
};

module.exports = new wunData(); 
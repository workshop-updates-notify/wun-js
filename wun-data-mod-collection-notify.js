const wunData = require('./wun-data.js'); 
const util = require('util');
const wunString = require('./wun-string.js');

var wunModColNotify = function() {};

wunModColNotify.prototype.addModColNotify = async function (guildId, channelId, modCollectionId, notifyText, creatorId) { 
    var conn = await wunData.getConnection(); 

    var addResult = 0;
    try {
        addResult = await conn.query(`INSERT INTO mod_collection_notify (guild_id, channel_id, mod_collection_id, notify_text, creator_id) VALUES (${guildId},${channelId},${modCollectionId},\'${notifyText}\',${creatorId})`); 
    }
    catch(err) {
        console.log(err); 
        throw Error(err);
    }
    return new Promise(resolve => { resolve(addResult > 0); }); 
};

wunModColNotify.prototype.removeModColNotify = async function (guildId, channelId, modCollectionId) {     
    var sqlCmd = `DELETE FROM mod_collection_notify WHERE guild_id=${guildId} AND channel_id=${channelId}`; 
    if (wunString.isNumberOnlyText(modCollectionId)) {
        sqlCmd += ` AND mod_collection_id=${modCollectionId}`;
    }
    var conn = await wunData.getConnection(); 

    var remResult = 0;
    try {
        remResult = await conn.query(sqlCmd); 
    }
    catch(err) {
        console.log(err); 
        throw Error(err);
    }
    return new Promise(resolve => { resolve(remResult > 0); }); 
};

wunModColNotify.prototype.showModColNotifies = async function (guildId) { 
    var conn = await wunData.getConnection(); 

    var modColNotifyResults = 0;
    try {
        modColNotifyResults = await conn.query(`SELECT RTRIM(CAST(guild_id AS CHAR(20))) as guild_id, ` + 
                                                        `RTRIM(CAST(channel_id AS CHAR(20))) as channel_id, ` + 
                                                        `RTRIM(CAST(mod_collection_id AS CHAR(20))) as mod_collection_id, ` + 
                                                        `RTRIM(CAST(creator_id AS CHAR(20))) as creator_id, ` + 
                                                        `notify_text, create_date ` + 
                                                        `FROM guild_auth_users WHERE guild_id=${guildId}`); 
    }
    catch(err) {
        console.log(err); 
        throw Error(err);
    }
    return new Promise(resolve => { resolve(modColNotifyResults); }); 
};

module.exports = new wunModColNotify();
//const token = process.env.WUN_BOT_TOKEN; // 'NTQ5NTA5MDIxOTM4MjIxMDYy.D1-Vdw.PZ7jTfDlxP2Z5kQfZFVfCKL5ANk';
const token = 'NTQ5NTA5MDIxOTM4MjIxMDYy.D1-Vdw.PZ7jTfDlxP2Z5kQfZFVfCKL5ANk';
const Discord = require('discord.js');
const readline = require('readline');
const normalizeWhitespace = require('normalize-html-whitespace');
const jslinq = require('jslinq');
const stringArgv = require('string-argv');
const wunString = require('.\\wun-string.js');
const wunAuthUsers = require('.\\wun-data-guild-auth-users.js'); 
const wunModColNotify = require('.\\wun-data-mod-collection-notify.js'); 

async function consoleInput(query) {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    return new Promise(resolve => rl.question(query, ans => {
        rl.close();
        resolve(ans);
    }))
}
async function commandLoop() { 
    console.log('');
    var cmd = await consoleInput("WUN-BOT-LISTENER. Enter Command: \r\n");
    console.log(cmd); 
    if (cmd == 'exit') {
        process.exit(0);
    }
    commandLoop();
}

const client = new Discord.Client();

async function executeCommand(args, authType, msg) {
    var response = '';
    if (!args || args.length == 0) {
        response = 'You need to provide me with a valid command. Try using **help** or **/?**'; 
    }
    else {
        var cmd = args[0].toLowerCase(); 
        args = new jslinq(args).skip(1);
        switch (cmd)
        {
            case 'authadd':
            {
                if (authType == 'ADMIN') {
                    var userIds = args.where(function(a) { return wunString.isNumberOnlyText(a); });
                    if (userIds.count() > 0) { 
                        for(var i = 0; i < userIds.count(); i++) {
                            var addAuthResult = wunAuthUsers.addUserAuth(msg.guild.id, userIds.items[i], msg.author.id);
                        }
                        var dispUsers = userIds.select(function(a) { return `\r\n - <@${a}>`; });
                        response = `Added authorization for user(s): \r\n${dispUsers.items}`; 
                    }
                    else { 
                        response = 'No valid Discord User Ids supplied.'; 
                    }
                }
                else { 
                    response = 'Command [authadd] is only allowed by Admins.';
                }
                break;
            }
            case 'authremove':
            case 'authrem':
            {
                if (authType == 'ADMIN') {
                    var userIds = args.where(function(a) { return wunString.isNumberOnlyText(a); });
                    if (userIds.count() > 0) { 
                        for(var i = 0; i < userIds.count(); i++) {
                            var addAuthResult = wunAuthUsers.removeUserAuth(msg.guild.id, userIds.items[i], msg.author.id);
                        }
                        var dispUsers = userIds.select(function(a) { return `\r\n - <@${a}>`; });
                        response = `Removed authorization for user(s): \r\n${dispUsers.items}`; 
                    }
                    else { 
                        response = 'No valid Discord User Ids supplied.'; 
                    }
                }
                else { 
                    response = 'Command [authremove] is only allowed by Admins.';
                }
                break;
            }
            case 'authshow': 
            {
                var authUsersRaw = await wunAuthUsers.showAuthUsers(msg.guild.id); 
                var authUsers = new jslinq(authUsersRaw).select(function(r) { return `\r\n - <@${r.user_id}>\t\t Granted by <@${r.creator_id}> on ${r.create_date}`; }).items;
                response = `The following users have authorization to configure Mod Collection Notifications. \r\n${authUsers.length > 0 ? authUsers : '\r\nNone'}`; 
                break;
            }
            case 'modadd':
            {
                var channelId = args.items[0];
                var modColId = args.items[1];
                var notifyText = args.items[2];
                response = `**COMING SOON** _(see ***help***)_\r\n\r\n` + 
                           `Your parameters were: \r\n\`\`\`` + 
                           `{channelId}       - ${channelId}\r\n` + 
                           `{modCollectionId} - ${modColId}\r\n` + 
                           `{notifyText}      - ${notifyText}\r\n\`\`\``;
                break;
            }
            case 'modremove':
            case 'modrem': 
            {
                var channelId = args.items[0];
                var modColId = args.items[1];
                response = `**COMING SOON** _(see ***help***)_\r\n\r\n` + 
                           `Your parameters were: \r\n\`\`\`` + 
                           `{channelId}       - ${channelId}\r\n` + 
                           `{modCollectionId} - ${modColId}\r\n\`\`\``;
                break;
            }
            case 'modshow':
            {
                response = '**COMING SOON** _(see ***help***)_'; 
                break;
            }
            case 'help':
            case '/?':
            {
                response = [
                            'Here is a list of valid commands. \r\n' + 
                            '```\r\n\r\n' + 
                            ' authadd        Add authorization for non-admin users to configure Mod Collection Notifications.\r\n' +
                            '                Expects 1 or more Discord User IDs, separated by space. \r\n' + 
                            '                Command Authorization: ADMINS Only\r\n\r\n' + 
                            ' authremove     Remove authorization for non-admin users from configuring Mod Collection Notifications.\r\n' + 
                            ' authrem        Expects 1 or more Discord User IDs, separated by space. \r\n' + 
                            '                Command Authorization: ADMINS Only\r\n\r\n' + 
                            ' authshow       Display all users that have been granted authorization to configure Mod Collection Notifications.\r\n' + 
                            '                Command Authorization: ADMINS or GRANTED\r\n\r\n' + 
                            ' help           Displays this help text. Alternate command: /?\r\n\r\n```',

                            'commands continued...``` **COMING SOON**\r\n\r\n' + 
                            ' modadd         Add a Mod Collection Notification for a channel on your Discord server.\r\n' + 
                            '                Expects 2 or 3 parameters separated by space: {channelId} {modCollectionId} "{notifyText}"\r\n' + 
                            '                {channelId}       - The Id for the channel you want to receive \r\n' + 
                            '                                    Mod Update Notifications on.\r\n' + 
                            '                {modCollectionId} - Steam\'s Id for the Mod Collection/Bundle that \r\n' + 
                            '                                    you would like watched for updates.\r\n' + 
                            '                (optional)\r\n' + 
                            '                {notifyText}      - Wrap in "double quotes" if this will contain any spaces!\r\n' + 
                            '                                    This text will appear at the top of any notifications sent.\r\n' + 
                            '                                    Recommended usage is for @mentions of Server Admin(s).\r\n' + 
                            '                                    @mentions need to be in the format: <@userId>\r\n\r\n' + 
                            ' modremove      Remove a Mod Collection Notification.\r\n' + 
                            ' modrem         Expects 1 or 2 parameters separated by space: {channelId} {modCollectionId}\r\n' + 
                            '                {channelId}       - The Id for the channel to remove notification(s) for.\r\n' + 
                            '                                    If this is used without the optional {modCollectionId}\r\n' + 
                            '                                    parameter all notifications for this channel will be removed.\r\n' + 
                            '                (optional)\r\n' + 
                            '                {modCollectionId} - Steam\'s Id for the Mod Collection/Bundle that \r\n' + 
                            '                                    you would like to remove notifications for.\r\n\r\n' + 
                            ' modshow        Display List of all configured Mod Collection Notifications for this Discord Server.\r\n\r\n' + 
                            '\r\n```'
                            ];
                break;
            }
            default:
            {
                if (cmd.startsWith('how') || 
                    cmd.startsWith('when') || 
                    cmd.startsWith('is') || 
                    cmd.startsWith('why') || 
                    cmd.startsWith('where') || 
                    cmd.startsWith('what'))
                {
                    response = "42\r\n\r\n... or ask for **help**.";   
                }
                else
                {
                    response = "blah-blah-blah. Get yourself some **help** or maybe try **/?**";
                }
            }
        }
    }
    return new Promise(resolve => { resolve(response); });
}

async function handleDiscordCommand(msg) {

    const channelId = msg.channel.id; 
    const guildId = msg.guild.id;
    const userId = msg.author.id; 
    const content = msg.content;
    console.log(`\r\nMsg Received - GuildId: ${guildId} ChannelId: ${channelId} UserId: ${userId} Content: ${content}`); 

    var isAdmin = msg.member.hasPermission('ADMINISTRATOR'); 
    var isOtherAuth = false;
    if (!isAdmin) {
        isOtherAuth = await wunAuthUsers.isUserAuthorized(guildId, userId);
    }

    var authType = '';
    if (isAdmin) { 
        authType = 'ADMIN';
    }
    else {
        authType = 'GRANTED';
    }

    if (isAdmin || isOtherAuth) {
        console.log(`\r\nUser Is Authorized. AuthType: ${authType}`);
        
        var tmpArgs = null;
        if (content.startsWith('!wun')) {
            tmpArgs = normalizeWhitespace(content.substring(4).trim());
        } 
        else { 
            tmpArgs = normalizeWhitespace(content.replace(`<@${client.user.id}>`, '').trim());
        }
        var args = null;
        if (tmpArgs == '') { 
            args = [];
        }
        else {
            //args = tmpArgs.split(/\s+/); 
            args = stringArgv(tmpArgs); 
            console.log(`Arguments: ${args.join(', ')}`);
        }
        var response = await executeCommand(args, authType, msg); 
        if (Array.isArray(response)) {
            for(var i = 0; i < response.length; i++) {
                await msg.reply(response[i]); 
                console.log(response[i]);
            }
        }
        else {
            await msg.reply(response);
            console.log(response);
        }
    }
    else {
        console.log('\r\nUser Is NOT Authorized.');
        await msg.reply('You are NOT authorized to make commands of me. Holler at an ADMIN.'); 
    }

    return new Promise(resolve => { resolve(true); });
}

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
  commandLoop();
});

client.on('guildCreate', guild => {
    console.log(`Notified of Guild - Id: ${guild.id}, Name: ${guild.name}`);
});

client.on('message', msg => {
    // don't pay attention to system or bot messages at all 
    if (msg.system || 
        msg.author.bot) {
        return;
    }
    // make sure this bot has been asked to do something... 
    if (msg.content.startsWith('!wun') || 
        msg.isMemberMentioned(client.user)) {
        handleDiscordCommand(msg); 
    }
});

client.on('rateLimit', rateLimitInfo => {
    console.log(`Discord Rate Limit: ${rateLimitInfo}`);
})

client.on('error', errorInfo => {
    console.log(`Discord Error: ${errorInfo}`);
})

client.login(token);




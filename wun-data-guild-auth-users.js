const wunData = require('.\\wun-data.js'); 
const util = require('util');

var wunAuthUsers = function() {};

wunAuthUsers.prototype.isUserAuthorized = async function (guildId, userId) {
    var conn = await wunData.getConnection(); 

    var userAuthResult = 0;
    try {
        userAuthResult = await conn.query(`SELECT Count(Id) FROM guild_auth_users WHERE guild_id=${guildId} AND user_id=${userId}`); 
    }
    catch(err) {
        console.log(err); 
        throw Error(err);
    }
    return new Promise(resolve => { resolve(userAuthResult > 0); }); 

};

wunAuthUsers.prototype.addUserAuth = async function (guildId, userId, creatorId) { 
    var conn = await wunData.getConnection(); 

    var addAuthResult = 0;
    try {
        addAuthResult = await conn.query(`INSERT INTO guild_auth_users (guild_id, user_id, creator_id) VALUES (${guildId},${userId},${creatorId})`); 
    }
    catch(err) {
        console.log(err); 
        throw Error(err);
    }
    return new Promise(resolve => { resolve(addAuthResult > 0); }); 
};

wunAuthUsers.prototype.removeUserAuth = async function (guildId, userId) { 
    var conn = await wunData.getConnection(); 

    var remAuthResult = 0;
    try {
        remAuthResult = await conn.query(`DELETE FROM guild_auth_users WHERE guild_id=${guildId} AND user_id=${userId}`); 
    }
    catch(err) {
        console.log(err); 
        throw Error(err);
    }
    return new Promise(resolve => { resolve(remAuthResult > 0); }); 
};

wunAuthUsers.prototype.showAuthUsers = async function (guildId) { 
    var conn = await wunData.getConnection(); 

    var authUsersResults = 0;
    try {
        authUsersResults = await conn.query(`SELECT RTRIM(CAST(user_id AS CHAR(20))) as user_id, RTRIM(CAST(creator_id AS CHAR(20))) as creator_id, create_date FROM guild_auth_users WHERE guild_id=${guildId}`); 
    }
    catch(err) {
        console.log(err); 
        throw Error(err);
    }
    return new Promise(resolve => { resolve(authUsersResults); }); 
};

module.exports = new wunAuthUsers();